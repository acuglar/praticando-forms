import React from "react";
import './App.css';

import Form from './components/form'

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Form />
      </header>
    </div>
  );
}


