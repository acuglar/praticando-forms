import { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { Styleform } from "./styles";

const Form = () => {
  const [isSuccess, setIsSuccess] = useState(false);

  const req = "Campo obrigatório!";
  const invE = "Email inválido";

  const schema = yup.object().shape({
    name: yup
      .string()
      .max(18, "Nome deve conter pelo menos 18 caracteres!")
      .required(req),
    age: yup.number().min(18).required(req),
    /* phone: yup.number().min(8),
    cep: yup.number().min(8), */
    email: yup.string().email(invE).required(req),
    emailConfirm: yup
      .string()
      .oneOf([yup.ref("email")])
      .required(req),
    password: yup
      .string()
      .min(8, "Mínimo 8 dígitos")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos ume letra maiúscula, uma minúscula, um número e um caractere especial!"
      )
      .required(req),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleData = (data) => {
    console.log(data);
    setIsSuccess(true);
    reset();
  };

  return (
    <div>
      <h1>Form validation</h1>
      <Styleform className="form" onSubmit={handleSubmit(handleData)}>
        <div>
          <input placeholder="Nome" type="text" name="name" ref={register} />
          <div>{errors.name?.message}</div>
          <input placeholder="Idade" type="number" name="age" ref={register} />
          <div>{errors.age?.message}</div>
        </div>
        {/*  <div>
          <input
            placeholder="Número para contato"
            type="text"
            name="phone"
            ref={register}
          />
          <input placeholder="CEP" type="text" name="cep" ref={register} />
        </div> */}
        <div>
          <input placeholder="Email" type="email" name="email" ref={register} />
          <div>{errors.email?.message}</div>
        </div>
        <div>
          <input
            placeholder="Confirme o email"
            type="text"
            name="emailConfirm"
            ref={register}
          />
          <div>{errors.emailConfirm?.message}</div>
        </div>
        <div>
          <input
            placeholder="Senha"
            type="password"
            name="password"
            ref={register}
          />
          <div>{errors.password?.message}</div>
          <button type="submit">Enviar</button>
        </div>
      </Styleform>
      {isSuccess && <div style={{ color: "green" }}>Enviado com sucesso!!</div>}
    </div>
  );
};

export default Form;

/* https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_popup */
