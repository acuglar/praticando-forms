import styled from 'styled-components';

export const Styleform = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 750px;

  input {
    margin: 7px 0;
    height: 25px;
    border-radius: 4px;
  }

  button {
    width: 180px;
    cursor: pointer;
    height: 30px;
    margin-top: 15px;
    background-color: #66c3ff;
    border: 0;
    color: #000;
  }
`;